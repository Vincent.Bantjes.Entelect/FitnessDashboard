package za.co.entelect.fitnessdashboard.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import za.co.entelect.fitnessdashboard.services.config.ServiceConfig;

@Configuration
@Import({ServiceConfig.class})
public class SpringRootConfig { }