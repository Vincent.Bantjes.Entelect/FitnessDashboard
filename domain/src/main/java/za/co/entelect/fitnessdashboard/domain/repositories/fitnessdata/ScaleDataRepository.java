package za.co.entelect.fitnessdashboard.domain.repositories.fitnessdata;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.ScaleData;

@Repository
public interface ScaleDataRepository extends CrudRepository<ScaleData, Long> { }