package za.co.entelect.fitnessdashboard.domain.repositories.devices;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.devices.HeartRateMonitor;

@Repository
public interface HeartRateMonitorRepository extends CrudRepository<HeartRateMonitor, Long> { }
