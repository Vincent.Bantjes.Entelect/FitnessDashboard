package za.co.entelect.fitnessdashboard.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.domain.objects.account.Gender;
import za.co.entelect.fitnessdashboard.domain.objects.security.User;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;
import za.co.entelect.fitnessdashboard.services.service.AccountService;
import za.co.entelect.fitnessdashboard.services.service.SecurityService;
import za.co.entelect.fitnessdashboard.web.models.AccountForm;

import java.time.LocalDateTime;
import java.util.Map;

@Controller
public class RegisterController
{
    @GetMapping("/register")
    public String registration(@ModelAttribute("accountForm") AccountForm accountForm,  BindingResult result, Model model)
    {
        return "register";
    }

    @PostMapping("/register")
    public final String processLogin(@ModelAttribute("accountForm") AccountForm accountForm,
                                     BindingResult result, Model model)
    {
        logger.info("Register processing...");

        accountFormValidator.validate(accountForm, result);
        if (result.hasErrors())
        {
            model.addAttribute(BindingResult.class.getName() + ".accountForm", result);
            return "register";
        }

        try
        {
            User user = new User();
            user.setUsername(accountForm.getUsername());
            user.setPassword(passwordEncoder.encode(accountForm.getPassword()));
            user.setSignedUp(LocalDateTime.now());

            Account account = new Account();
            account.setGender(accountForm.getGender().equals("male") ? Gender.MALE : Gender.FEMALE);
            account.setUser(user);
            account.setAge(Integer.valueOf(accountForm.getAge()));

            AccountService.RegisterResponse response = accountService.register(new AccountService.
                    RegisterRequest(account));
            if (response.getAccount() != null)
            {
                model.addAttribute("account", response.getAccount());

                securityService.login(accountForm.getUsername(), accountForm.getPassword());

                return "account";
            }
            else
            {
                model.addAttribute("message", "Something went wrong. Please try again.");
                return "redirect:/register";
            }
        }
        catch(PreconditionViolationException | AccountService.AccountAlreadyExistsException exception)
        {
            logger.error("{}", exception.getMessage());
            model.addAttribute("message", "Something went wrong. Please try again." + exception.getMessage());
            return "redirect:/register";
        }
    }

    @Autowired
    private AccountFormValidator accountFormValidator;

    @Autowired
    private AccountService accountService;

    @Autowired
    private SecurityService securityService;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private final Logger logger = LoggerFactory.getLogger(RegisterController.class);
}