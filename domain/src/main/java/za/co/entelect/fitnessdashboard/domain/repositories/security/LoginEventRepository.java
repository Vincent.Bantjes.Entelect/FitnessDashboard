package za.co.entelect.fitnessdashboard.domain.repositories.security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.security.LoginEvent;

@Repository
public interface LoginEventRepository extends CrudRepository<LoginEvent, Long> { }