package za.co.entelect.fitnessdashboard.services.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import za.co.entelect.fitnessdashboard.domain.objects.goals.Fitness;
import za.co.entelect.fitnessdashboard.domain.objects.goals.WeightLoss;
import za.co.entelect.fitnessdashboard.domain.repositories.account.AccountRepository;
import za.co.entelect.fitnessdashboard.domain.repositories.goals.FitnessRepository;
import za.co.entelect.fitnessdashboard.domain.repositories.goals.WeightlossRepository;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

@Configuration
public class GoalServiceImpl extends UtilityService implements GoalService
{
    @Override
    @Transactional
    public AddGoalResponse addGoal(AddGoalRequest addGoalRequest)
            throws PreconditionViolationException, GoalAlreadyExistsException, GoalNotSupportedException
    {
        // Validate input
        validateRequest(addGoalRequest, logger);

        if(addGoalRequest.getGoal() instanceof Fitness)
        {
            return new AddGoalResponse(fitnessDao.save((Fitness)addGoalRequest.getGoal()));
        }
        else if(addGoalRequest.getGoal() instanceof WeightLoss)
        {
            return new AddGoalResponse(weightlossDao.save((WeightLoss)addGoalRequest.getGoal()));
        }
        else
        {
            throw new GoalNotSupportedException("The given goal is not supported yet.");
        }
    }

    @Override
    @Transactional
    public AddGoalToAccountResponse addGoalToAccount(AddGoalToAccountRequest addGoalToAccountRequest) throws
            PreconditionViolationException, GoalAlreadyExistsException
    {
        // Validate input
        validateRequest(addGoalToAccountRequest, logger);

        // Validate that the goal does not already exist.
        if(addGoalToAccountRequest.getAccount().getGoalsLinkedToAccount().contains(addGoalToAccountRequest.getGoal()))
        {
            throw new GoalAlreadyExistsException("The goal " + addGoalToAccountRequest.getGoal().getClass().getName().
                    substring(addGoalToAccountRequest.getGoal().getClass().getName().lastIndexOf(".") + 1) +
                    " already exists.");
        }

        // Add the goal if it does not exist.
        addGoalToAccountRequest.getAccount().getGoalsLinkedToAccount().add(addGoalToAccountRequest.getGoal());

        return new AddGoalToAccountResponse(accountDao.save(addGoalToAccountRequest.getAccount()));
    }

    @Override
    @Transactional
    public RemoveGoalFromAccountResponse removeGoalFromAccount(RemoveGoalFromAccountRequest
            removeGoalFromAccountRequest) throws PreconditionViolationException, GoalDoesNotExistsException
    {
        // Validate input
        validateRequest(removeGoalFromAccountRequest, logger);

        // Validate that the goal does already exist.
        if(!removeGoalFromAccountRequest.getAccount().getGoalsLinkedToAccount().contains(removeGoalFromAccountRequest.
                getGoal()))
        {
            throw new GoalDoesNotExistsException("The goal " + removeGoalFromAccountRequest.getGoal().getClass().
                    getName().substring(removeGoalFromAccountRequest.getGoal().getClass().getName().lastIndexOf(".")
                    + 1) + " does not exists exists.");
        }

        // Remove the goal if it does not exist.
        removeGoalFromAccountRequest.getAccount().getGoalsLinkedToAccount().add(removeGoalFromAccountRequest.getGoal());

        return new RemoveGoalFromAccountResponse(accountDao.save(removeGoalFromAccountRequest.getAccount()));
    }

    private Logger logger = LoggerFactory.getLogger(GoalServiceImpl.class);

    @Autowired
    private AccountRepository accountDao;

    @Autowired
    private FitnessRepository fitnessDao;

    @Autowired
    private WeightlossRepository weightlossDao;
}