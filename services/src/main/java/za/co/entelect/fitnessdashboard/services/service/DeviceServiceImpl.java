package za.co.entelect.fitnessdashboard.services.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import za.co.entelect.fitnessdashboard.domain.repositories.account.AccountRepository;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

@Configuration
public class DeviceServiceImpl extends UtilityService implements DeviceService
{
    @Override
    @Transactional
    public AddDeviceToAccountResponse addDeviceToAccount(AddDeviceToAccountRequest addDeviceToAccountRequest)
            throws PreconditionViolationException, DeviceAlreadyExistsException
    {
        // Validate input
        validateRequest(addDeviceToAccountRequest, logger);

        // Validate that the device does not already exist.
        if(addDeviceToAccountRequest.getAccount().getDevicesLinkedToAccount().contains(addDeviceToAccountRequest.getDevice()))
        {
            throw new DeviceAlreadyExistsException("The device " + addDeviceToAccountRequest.getDevice().getClass().getName().
                    substring(addDeviceToAccountRequest.getDevice().getClass().getName().lastIndexOf(".") + 1) +
                    " already exists.");
        }

        // Add the device if it does not exist.
        addDeviceToAccountRequest.getAccount().getDevicesLinkedToAccount().add(addDeviceToAccountRequest.getDevice());

        return new AddDeviceToAccountResponse(accountDao.save(addDeviceToAccountRequest.getAccount()));
    }

    @Override
    @Transactional
    public RemoveDeviceFromAccountResponse removeDeviceFromAccount(RemoveDeviceFromAccountRequest
            removeDeviceFromAccountRequest) throws PreconditionViolationException, DeviceDoesNotExistsException
    {
        // Validate input
        validateRequest(removeDeviceFromAccountRequest, logger);

        // Validate that the device does already exist.
        if(!removeDeviceFromAccountRequest.getAccount().getDevicesLinkedToAccount().contains(
                removeDeviceFromAccountRequest.getDevice()))
        {
            throw new DeviceDoesNotExistsException("The device " + removeDeviceFromAccountRequest.getDevice().
                    getClass().getName().substring(removeDeviceFromAccountRequest.getDevice().getClass().getName().
                    lastIndexOf(".") + 1) + " does not exists exists.");
        }

        // Remove the device if it does not exist.
        removeDeviceFromAccountRequest.getAccount().getDevicesLinkedToAccount().add(removeDeviceFromAccountRequest.
                getDevice());

        return new RemoveDeviceFromAccountResponse(accountDao.save(removeDeviceFromAccountRequest.getAccount()));
    }

    private Logger logger = LoggerFactory.getLogger(DeviceServiceImpl.class);

    @Autowired
    private AccountRepository accountDao;
}