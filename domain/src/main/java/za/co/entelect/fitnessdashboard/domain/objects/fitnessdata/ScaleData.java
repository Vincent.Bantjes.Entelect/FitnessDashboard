package za.co.entelect.fitnessdashboard.domain.objects.fitnessdata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "SCALE_DATA")
@DiscriminatorValue(value = "SCALE_DATA")
public class ScaleData extends FitnessData
{
    @Column(name = "WEIGHT")
    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @NotNull(message = "The weight must be specified.")
    private Integer weight;
}
