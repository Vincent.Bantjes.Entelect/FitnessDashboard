package za.co.entelect.fitnessdashboard.domain.objects.fitnessdata;

import za.co.entelect.fitnessdashboard.domain.objects.base.BaseEntity;
import za.co.entelect.fitnessdashboard.domain.objects.converter.LocalDateTimeAttributeConverter;
import za.co.entelect.fitnessdashboard.domain.objects.devices.Device;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "FITNESS_DATA")
@DiscriminatorColumn(name = "FITNESS_DATA_TYPE", discriminatorType = DiscriminatorType.STRING, length = 80)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class FitnessData extends BaseEntity
{
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    @Column(name = "CAPTURE_DATE")
    public LocalDateTime getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(LocalDateTime captureDate) {
        this.captureDate = captureDate;
    }

    @ManyToOne
    @JoinColumn(name = "DEVICE")
    public Device getDevice() { return device; }

    public void setDevice(Device device) {
        this.device = device;
    }

    @NotNull(message = "The date that the fitness data was captured must be specified.")
    private LocalDateTime captureDate;

    @Valid
    @NotNull(message = "The device for the fitness data must be specified.")
    private Device device;
}