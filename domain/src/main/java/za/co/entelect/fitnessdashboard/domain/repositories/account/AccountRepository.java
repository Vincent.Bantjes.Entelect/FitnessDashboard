package za.co.entelect.fitnessdashboard.domain.repositories.account;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;

@Repository
public interface AccountRepository extends PagingAndSortingRepository<Account, Long>
{
    @Query("SELECT a FROM User u JOIN Account a ON a.user = u.systemId WHERE u.username = :username")
    Account findByUsername(@Param("username") String username);
}