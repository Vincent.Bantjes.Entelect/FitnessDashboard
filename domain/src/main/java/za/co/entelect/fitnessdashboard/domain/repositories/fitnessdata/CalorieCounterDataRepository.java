package za.co.entelect.fitnessdashboard.domain.repositories.fitnessdata;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.CalorieCounterData;

@Repository
public interface CalorieCounterDataRepository extends CrudRepository<CalorieCounterData, Long> { }