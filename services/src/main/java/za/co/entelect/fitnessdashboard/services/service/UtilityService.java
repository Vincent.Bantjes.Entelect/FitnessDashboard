package za.co.entelect.fitnessdashboard.services.service;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public abstract class UtilityService
{
    protected void validateRequest(Object object, Logger logger) throws PreconditionViolationException
    {
        String violations = null;
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        if(object instanceof CalorieCalculator.CalculateCaloriesBurntRequest)
        {
            Set<ConstraintViolation<CalorieCalculator.CalculateCaloriesBurntRequest>> violationsSet = validator.validate(
                    (CalorieCalculator.CalculateCaloriesBurntRequest)object);

            if(!violationsSet.isEmpty())
            {
                for (ConstraintViolation<CalorieCalculator.CalculateCaloriesBurntRequest> violation : violationsSet)
                {
                    logger.error(violation.getMessage());
                    if(Strings.isNullOrEmpty(violations))
                    {
                        violations = violation.getMessage();
                    }
                    else
                    {
                        violations += " " + violation.getMessage();
                    }
                }
                throw new PreconditionViolationException(violations);
            }
        }
        else if(object instanceof SecurityService.LoginRequest)
        {
            Set<ConstraintViolation<SecurityService.LoginRequest>> violationsSet = validator.validate(
                    (SecurityService.LoginRequest)object);

            if(!violationsSet.isEmpty())
            {
                for (ConstraintViolation<SecurityService.LoginRequest> violation : violationsSet)
                {
                    logger.error(violation.getMessage());
                    if (Strings.isNullOrEmpty(violations))
                    {
                        violations = violation.getMessage();
                    }
                    else
                    {
                        violations += " " + violation.getMessage();
                    }
                }
                throw new PreconditionViolationException(violations);
            }
        }
        else if(object instanceof SecurityService.CheckIfAccountExistsRequest)
        {
            Set<ConstraintViolation<SecurityService.CheckIfAccountExistsRequest>> violationsSet = validator.validate(
                    (SecurityService.CheckIfAccountExistsRequest)object);

            if(!violationsSet.isEmpty())
            {
                for (ConstraintViolation<SecurityService.CheckIfAccountExistsRequest> violation : violationsSet)
                {
                    logger.error(violation.getMessage());
                    if (Strings.isNullOrEmpty(violations))
                    {
                        violations = violation.getMessage();
                    }
                    else
                    {
                        violations += " " + violation.getMessage();
                    }
                }
                throw new PreconditionViolationException(violations);
            }
        }
        else if(object instanceof GenerateData.GenerateDevicesRequest)
        {
            Set<ConstraintViolation<GenerateData.GenerateDevicesRequest>> violationsSet = validator.validate(
                    (GenerateData.GenerateDevicesRequest)object);

            if(!violationsSet.isEmpty())
            {
                for (ConstraintViolation<GenerateData.GenerateDevicesRequest> violation : violationsSet)
                {
                    logger.error(violation.getMessage());
                    if (Strings.isNullOrEmpty(violations))
                    {
                        violations = violation.getMessage();
                    }
                    else
                    {
                        violations += " " + violation.getMessage();
                    }
                }
                throw new PreconditionViolationException(violations);
            }
        }
    }
}
