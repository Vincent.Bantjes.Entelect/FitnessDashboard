package za.co.entelect.fitnessdashboard.web.controller;

import org.h2.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import za.co.entelect.fitnessdashboard.web.models.AccountForm;

@Component
@PropertySource("classpath:/messages.properties")
public class AccountFormValidator implements Validator
{
    @Override
    public boolean supports(Class<?> clazz)
    {
        return AccountForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors)
    {
        AccountForm accountForm = (AccountForm) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", environment.getProperty("NotEmpty.username"));
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", environment.getProperty("NotEmpty.password"));
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "gender", environment.getProperty("NotEmpty.gender"));
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "age", environment.getProperty("NotEmpty.age"));

        if(!StringUtils.isNumber(accountForm.getAge()))
        {
            errors.rejectValue("age", environment.getProperty("MustBeANumber.age"));
        }

        if(accountForm.getAge() == null || Integer.valueOf(accountForm.getAge()).intValue() <= 0)
        {
            errors.rejectValue("age", environment.getProperty("NonNegative.age"));
        }
    }

    @Autowired
    private Environment environment;

    private Logger logger = LoggerFactory.getLogger(AccountFormValidator.class);
}