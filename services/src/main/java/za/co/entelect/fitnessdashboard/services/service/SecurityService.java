package za.co.entelect.fitnessdashboard.services.service;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Service;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Service
public interface SecurityService
{
    LoginResponse login(LoginRequest loginRequest) throws PreconditionViolationException;

    class LoginRequest
    {
        public LoginRequest() { }

        public LoginRequest(String username, String password)
        {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @NotEmpty(message = "The username must be specified.")
        private String username;

        @NotNull(message = "The password must be specified.")
        @Size(min = 1, message = "The password must be specified.")
        private String password;
    }

    class LoginResponse
    {
        public LoginResponse(Boolean passwordResult) {
            this.passwordResult = passwordResult;
        }

        public Boolean getPasswordResult() {
            return passwordResult;
        }

        public void setPasswordResult(Boolean passwordResult) {
            this.passwordResult = passwordResult;
        }

        @NotNull(message = "The result of the login must be specified.")
        private Boolean passwordResult;
    }

    CheckIfAccountExistsResponse checkIfAccountExists(CheckIfAccountExistsRequest checkIfAccountExistsRequest)
            throws PreconditionViolationException;


    class CheckIfAccountExistsRequest
    {
        public CheckIfAccountExistsRequest(String username)
        {
            this.username = username;
        }

        public CheckIfAccountExistsRequest(Long id)
        {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public Long getId()
        {
            return id;
        }

        public void setId(Long id)
        {
            this.id = id;
        }

        private String username;
        private Long id;
    }

    class CheckIfAccountExistsResponse
    {
        public CheckIfAccountExistsResponse(Boolean checkResult) {
            this.checkResult = checkResult;
        }

        public Boolean getCheckResult() {
            return checkResult;
        }

        public void setCheckResult(Boolean checkResult) {
            this.checkResult = checkResult;
        }

        @NotNull(message = "The result of the check must be specified.")
        private Boolean checkResult;
    }

    Boolean login(String username, String password);

    String findLoggedInUsername();
}
