package za.co.entelect.fitnessdashboard.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;
import za.co.entelect.fitnessdashboard.services.service.AccountService;
import za.co.entelect.fitnessdashboard.services.service.GoalService;

import java.util.Map;

@Controller
public class GoalController
{
    @GetMapping("/goal")
    public String processAccount(Authentication authentication, Map<String, Object> model) //
    {
        logger.info("View Account processing...");
        try
        {
            AccountService.RetrieveAccountByUsernameResponse retrieveAccountByUsernameResponse =
                    accountService.retrieveAccountByUsername(new AccountService.
                            RetrieveAccountByUsernameRequest(authentication.getName()));

            return "goal";
        }
        catch(PreconditionViolationException | AccountService.AccountDoesNotExistException exception)
        {
            logger.error("{}", exception.getMessage());
            model.put("message", exception.getMessage());
            return "/";
        }
    }

    @Autowired
    private AccountService accountService;

    @Autowired
    private GoalService goalService;

    private final Logger logger = LoggerFactory.getLogger(GoalController.class);
}