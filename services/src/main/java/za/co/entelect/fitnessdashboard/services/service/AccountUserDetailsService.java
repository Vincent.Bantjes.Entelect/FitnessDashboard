package za.co.entelect.fitnessdashboard.services.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import za.co.entelect.fitnessdashboard.domain.objects.security.User;
import za.co.entelect.fitnessdashboard.domain.repositories.security.UserRepository;
import za.co.entelect.fitnessdashboard.domain.repositories.security.UserRoleRepository;
import za.co.entelect.fitnessdashboard.services.security.AccountUserDetails;

import java.util.List;

@Service(value = "AccountUserDetailsService")
public class AccountUserDetailsService implements UserDetailsService
{
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        User user = userDao.findByUsername(username);

        if(null == user)
        {
            throw new UsernameNotFoundException("No user present with username: " + username);
        }
        else
        {
            List<String> userRoles = userRoleDao.findRoleByUsername(username);
            return new AccountUserDetails(user, userRoles);
        }
    }

    @Autowired
    private UserRepository userDao;

    @Autowired
    private UserRoleRepository userRoleDao;
}
