package za.co.entelect.fitnessdashboard.web.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;

@Controller
public class WelcomeController
{
    @GetMapping("/")
    public String root(Map<String, Object> model)
    {
        model.put("message", this.message);
        return "home";
    }

    @GetMapping("/home")
    public String home(Map<String, Object> model)
    {
        model.put("message", this.message);
        return "home";
    }

    @GetMapping("/welcome")
    public String welcome(Map<String, Object> model)
    {
        model.put("message", this.message);
        return "home";
    }

    @GetMapping("/about")
    public String about() {
        return "/about";
    }

    // inject via application.properties
    @Value("${welcome.message:test}")
    private String message = "Hello World";
}