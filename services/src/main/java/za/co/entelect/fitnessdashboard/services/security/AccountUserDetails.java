package za.co.entelect.fitnessdashboard.services.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import za.co.entelect.fitnessdashboard.domain.objects.security.User;

import java.util.Collection;
import java.util.List;

public class AccountUserDetails extends User implements UserDetails
{
    public AccountUserDetails(User user, List<String> userRoles)
    {
        super(user);
        this.userRoles=userRoles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities()
    {
        String roles = StringUtils.collectionToCommaDelimitedString(userRoles);
        return AuthorityUtils.commaSeparatedStringToAuthorityList(roles);
    }

    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    @Override
    public boolean isEnabled()
    {
        return true;
    }

    @Override
    public String getUsername()
    {
        return super.getUsername();
    }

    @Override
    public String getPassword()
    {
        return super.getPassword();
    }

    private List<String> userRoles;
}