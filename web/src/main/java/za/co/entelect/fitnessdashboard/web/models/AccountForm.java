package za.co.entelect.fitnessdashboard.web.models;

import org.hibernate.validator.constraints.NotEmpty;
import za.co.entelect.fitnessdashboard.domain.objects.account.Gender;

import javax.validation.constraints.NotNull;

public class AccountForm
{
    public AccountForm() { }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getAge()
    {
        return age;
    }

    public void setAge(String age)
    {
        this.age = age;
    }

    public Gender getGender()
    {
        return gender;
    }

    public void setGender(Gender gender)
    {
        this.gender = gender;
    }

    public String getRoles()
    {
        return roles;
    }

    public void setRoles(String roles)
    {
        this.roles = roles;
    }

    private Long id;
    @NotEmpty(message = "A username must be specified.")
    private String username;
    @NotEmpty(message = "A password must be specified.")
    private String password;
    @NotEmpty(message = "The roles must be specified.")
    private String roles;
    @NotEmpty(message = "An age must be specified.")
    private String age;
    @NotNull(message = "A gender must be specified.")
    private Gender gender;

}
