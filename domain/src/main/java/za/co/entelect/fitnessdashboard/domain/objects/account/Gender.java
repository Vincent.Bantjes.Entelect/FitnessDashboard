package za.co.entelect.fitnessdashboard.domain.objects.account;

public enum Gender
{
    MALE("Male"), FEMALE("Female");

    private final String displayName;

    Gender(String displayName)
    {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString()
    {
        return super.toString();
    }
}
