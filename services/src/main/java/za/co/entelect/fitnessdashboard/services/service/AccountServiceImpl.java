package za.co.entelect.fitnessdashboard.services.service;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.domain.objects.security.UserRole;
import za.co.entelect.fitnessdashboard.domain.repositories.account.AccountRepository;
import za.co.entelect.fitnessdashboard.domain.repositories.security.UserRepository;
import za.co.entelect.fitnessdashboard.domain.repositories.security.UserRoleRepository;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import java.util.List;

@Configuration
public class AccountServiceImpl extends UtilityService implements AccountService
{
    @Override
    @Transactional
    public RegisterResponse register(RegisterRequest registerRequest) throws PreconditionViolationException,
            AccountAlreadyExistsException
    {
        // Validate input
        validateRequest(registerRequest, logger);

        // Calculate calorie count
        if(securityService.checkIfAccountExists(new SecurityService.CheckIfAccountExistsRequest(
                registerRequest.getAccount().getUser().getUsername())).getCheckResult())
        {
            throw new AccountAlreadyExistsException("An account with the with the given details already exists.");
        }
        else
        {
            registerRequest.getAccount().setUser(userDao.save(registerRequest.getAccount().getUser()));
            // TODO Add default role
            UserRole userRole = new UserRole();
            userRole.setUserid(registerRequest.getAccount().getUser().getSystemId());
            userRole.setRole("ROLE_USER");
            userRoleDao.save(userRole);
            // TODO Add default role

            return new RegisterResponse(accountDao.save(registerRequest.getAccount()));
        }
    }

    @Override
    public RetrieveAccountByUsernameResponse retrieveAccountByUsername(RetrieveAccountByUsernameRequest
                                                                                   retrieveAccountByUsernameRequest)
            throws PreconditionViolationException, AccountDoesNotExistException
    {
        // Validate input
        validateRequest(retrieveAccountByUsernameRequest, logger);

        // Retrieve account
        if(!securityService.checkIfAccountExists(new SecurityService.CheckIfAccountExistsRequest(
                retrieveAccountByUsernameRequest.getUsername())).getCheckResult())
        {
            throw new AccountDoesNotExistException("An account with the with the given details does not exist.");
        }
        else
        {
            return new RetrieveAccountByUsernameResponse(accountDao.findByUsername(retrieveAccountByUsernameRequest.
                    getUsername()));
        }
    }

    @Override
    public RetrieveAccountByIdResponse retrieveAccountById(RetrieveAccountByIdRequest
            retrieveAccountByUsernameRequest) throws PreconditionViolationException,
            AccountDoesNotExistException
    {
        // Validate input
        validateRequest(retrieveAccountByUsernameRequest, logger);

        // Calculate calorie count
        if(!securityService.checkIfAccountExists(new SecurityService.CheckIfAccountExistsRequest(
                retrieveAccountByUsernameRequest.getId())).getCheckResult())
        {
            throw new AccountDoesNotExistException("An account with the with the given details does not exist.");
        }
        else
        {
            return new RetrieveAccountByIdResponse(accountDao.findOne(retrieveAccountByUsernameRequest.
                    getId()));
        }
    }

    @Override
    public RetrieveAllAccountsResponse retrieveAllAccounts(RetrieveAllAccountsRequest retrieveAllAccountsRequest) throws
            PreconditionViolationException
    {
        Iterable<Account> accounts = accountDao.findAll();

        RetrieveAllAccountsResponse retrieveAllAccountsResponse = new RetrieveAllAccountsResponse();
        for (Account account : accounts) {
            retrieveAllAccountsResponse.getAccounts().add(account);
        }

        return retrieveAllAccountsResponse;
    }

    @Override
    @Transactional
    public UpdateResponse update(UpdateRequest updateRequest) throws PreconditionViolationException,
            AccountAlreadyExistsException, AccountDoesNotExistException
    {
        // Validate input
        validateRequest(updateRequest, logger);

        // Calculate calorie count
        if(!securityService.checkIfAccountExists(new SecurityService.CheckIfAccountExistsRequest(
                updateRequest.getCurrentAccount().getUser().getUsername())).getCheckResult())
        {
            throw new AccountDoesNotExistException("An account with the with the given current details does not exist.");
        }
        else
        {
            // Update user details
            updateRequest.getCurrentAccount().getUser().setPassword(updateRequest.getNewAccountWithDetails().getUser()
                    .getPassword());

            updateRequest.getCurrentAccount().setUser(userDao.save(updateRequest.getCurrentAccount().getUser()));

            // Update account details
            updateRequest.getCurrentAccount().setAge(updateRequest.getNewAccountWithDetails().getAge());
            updateRequest.getCurrentAccount().setGender(updateRequest.getNewAccountWithDetails().getGender());

            return new UpdateResponse(accountDao.save(updateRequest.getCurrentAccount()));
        }
    }

    @Override
    public RetrieveRolesResponse retrieveRoles(RetrieveRolesRequest retrieveRolesRequest) throws PreconditionViolationException
    {
        List<String> roles = userRoleDao.findRoleByUsername(retrieveRolesRequest.getAccount().getUser().getUsername());

        RetrieveRolesResponse retrieveRolesResponse = new RetrieveRolesResponse();
        for (String role : roles)
        {
            if(Strings.isNullOrEmpty(retrieveRolesResponse.getRoles()))
            {
                retrieveRolesResponse.setRoles(role);
            }
            else
            {
                retrieveRolesResponse.setRoles(retrieveRolesResponse.getRoles() + "|" + role);
            }
        }
        return retrieveRolesResponse;
    }

    private Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Autowired
    private AccountRepository accountDao;

    @Autowired
    private UserRepository userDao;

    @Autowired
    private UserRoleRepository userRoleDao;

    @Autowired
    private SecurityService securityService;
}