package za.co.entelect.fitnessdashboard.domain.objects.goals;

import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.BodyFatIndicatorData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.CalorieCounterData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.ScaleData;

import javax.persistence.*;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "WEIGHT_LOSS")
@DiscriminatorValue(value = "WEIGHT_LOSS")
public class WeightLoss extends Goal
{
    @ManyToOne
    @JoinColumn(name = "BODY_FAT_GOAL")
    public BodyFatIndicatorData getBodyFatIndicatorGoal()
    {
        return bodyFatIndicatorGoal;
    }

    public void setBodyFatIndicatorGoal(BodyFatIndicatorData bodyFatIndicatorGoal)
    {
        this.bodyFatIndicatorGoal = bodyFatIndicatorGoal;
    }

    @ManyToOne
    @JoinColumn(name = "CALORIE_COUNTER_GOAL")
    public CalorieCounterData getCalorieCounterGoal()
    {
        return calorieCounterGoal;
    }

    public void setCalorieCounterGoal(CalorieCounterData calorieCounterGoal)
    {
        this.calorieCounterGoal = calorieCounterGoal;
    }

    @ManyToOne
    @JoinColumn(name = "WEIGHT_GOAL")
    public ScaleData getWeightGoal()
    {
        return weightGoal;
    }

    public void setWeightGoal(ScaleData weightGoal)
    {
        this.weightGoal = weightGoal;
    }

    @Valid
    private BodyFatIndicatorData bodyFatIndicatorGoal;

    @Valid
    private CalorieCounterData calorieCounterGoal;

    @Valid
    private ScaleData weightGoal;
}