package za.co.entelect.fitnessdashboard.domain.objects.devices;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Access(AccessType.PROPERTY)
@DiscriminatorValue(value = "SCALE")
public class Scale extends Device { }
