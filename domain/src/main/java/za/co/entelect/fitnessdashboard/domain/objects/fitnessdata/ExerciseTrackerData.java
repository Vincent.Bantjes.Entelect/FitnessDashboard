package za.co.entelect.fitnessdashboard.domain.objects.fitnessdata;

import za.co.entelect.fitnessdashboard.domain.objects.converter.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "EXERCISE_TRACKER_DATA")
@DiscriminatorValue(value = "EXERCISE_TRACKER_DATA")
public class ExerciseTrackerData extends FitnessData
{
    @Enumerated(EnumType.STRING)
    @Column(name = "EXERCISE_TYPE")
    public EXERCISE_TYPE getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(EXERCISE_TYPE exerciseType) {
        this.exerciseType = exerciseType;
    }

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    @Column(name = "START_DATE")
    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    @Column(name = "END_DATE")
    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public enum EXERCISE_TYPE
    {
        RUN, CYCLE, ROW, HIKING;
    }

    @NotNull(message = "The exercise type must be specified.")
    private EXERCISE_TYPE exerciseType;

    @NotNull(message = "The start date and time for the exercise must eb specified.")
    private LocalDateTime startDate;

    @NotNull(message = "The end date and time for the exercise must eb specified.")
    private LocalDateTime endDate;
}
