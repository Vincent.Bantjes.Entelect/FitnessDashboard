package za.co.entelect.fitnessdashboard.domain.repositories.devices;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.devices.ExerciseTracker;

@Repository
public interface ExerciseTrackerRepository extends CrudRepository<ExerciseTracker, Long> { }