package za.co.entelect.fitnessdashboard.domain.objects.fitnessdata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "PEDOMETER_DATA")
@DiscriminatorValue(value = "PEDOMETER_DATA")
public class PedometerData extends FitnessData
{
    @Column(name = "STEP_COUNT")
    public Integer getStepCount() {
        return stepCount;
    }

    public void setStepCount(Integer stepCount) {
        this.stepCount = stepCount;
    }

    @NotNull(message = "The step count must be specified.")
    private Integer stepCount;
}
