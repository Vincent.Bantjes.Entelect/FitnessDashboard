package za.co.entelect.fitnessdashboard.domain.repositories.security;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.security.UserRole;

import java.util.List;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole, Long>
{
    @Query("SELECT a.role FROM UserRole a, User b where b.username = ?1 and a.userid = b.systemId")
    List<String> findRoleByUsername(@Param("username") String username);
}