package za.co.entelect.fitnessdashboard.domain.objects.devices;

import za.co.entelect.fitnessdashboard.domain.objects.base.BaseEntity;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.FitnessData;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "DEVICE")
@DiscriminatorColumn(name = "DEVICE_TYPE", discriminatorType = DiscriminatorType.STRING, length = 80)
public abstract class Device extends BaseEntity
{
    @OneToMany
    @JoinColumn(name = "FITNESS_DATA")
    public List<FitnessData> getFitnessDataLinkedToDevice()
    {
        return fitnessDataLinkedToDevice;
    }

    public void setFitnessDataLinkedToDevice(List<FitnessData> fitnessDataLinkedToDevice) {
        this.fitnessDataLinkedToDevice = fitnessDataLinkedToDevice;
    }

    @Valid
    @NotNull(message = "The list of devices needs to be initialized.")
    private List<FitnessData> fitnessDataLinkedToDevice = new ArrayList();
}