package za.co.entelect.fitnessdashboard.domain.repositories.security;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.security.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>
{
    @Query("SELECT u FROM User u WHERE u.username = :username")
    User findByUsername(@Param("username") String username);
}