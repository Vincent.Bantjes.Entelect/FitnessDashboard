package za.co.entelect.fitnessdashboard.services.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.domain.objects.account.Gender;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.CalorieBurnData;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import java.time.temporal.ChronoUnit;

@Configuration
public class CalorieCalculatorImpl extends UtilityService implements CalorieCalculator
{
    @Override
    public CalorieBurnData calculateCaloriesBurnt(CalculateCaloriesBurntRequest calculateCaloriesBurntRequest)
            throws PreconditionViolationException
    {
        // Validate input
        validateRequest(calculateCaloriesBurntRequest, logger);

        // Response object
        CalorieBurnData calorieBurnDataResponse = new CalorieBurnData();

        // Calculate calorie count
        if(calculateCaloriesBurntRequest.getAccount().getGender().equals(Gender.MALE))
        {
            calorieBurnDataResponse.setCaloriesBurnt(calculationOfCalories(calculateCaloriesBurntRequest, maleConstantValues));
        }
        else
        {
            calorieBurnDataResponse.setCaloriesBurnt(calculationOfCalories(calculateCaloriesBurntRequest, femaleConstantValues));
        }

        return calorieBurnDataResponse;
    }

    private Double calculationOfCalories(CalculateCaloriesBurntRequest calculateCaloriesBurntRequest, double[] values)
    {
        return new Double((values[0] + (values[1] *
                calculateCaloriesBurntRequest.getHeartRateMonitorData().getHeartRate()) - (values[2] *
                calculateCaloriesBurntRequest.getScaleData().getWeight()) + (values[3] *
                calculateCaloriesBurntRequest.getAccount().getAge())) / values[4]) * 60 *
                calculateCaloriesBurntRequest.getExerciseTrackerData().getStartDate().until(calculateCaloriesBurntRequest.
                        getExerciseTrackerData().getEndDate(), ChronoUnit.HOURS);
    }

    private final double[] maleConstantValues = { -55.0969, 0.6309, 0.6309, 0.2017, 4.184 };
    private final double[] femaleConstantValues = { -20.4022, 0.4472, 0.1263, 0.074, 4.184 };

    private Logger logger = LoggerFactory.getLogger(CalorieCalculatorImpl.class);
}