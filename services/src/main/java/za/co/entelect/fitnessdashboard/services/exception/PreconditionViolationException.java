package za.co.entelect.fitnessdashboard.services.exception;

public class PreconditionViolationException extends Exception
{
    public PreconditionViolationException() {
    }

    public PreconditionViolationException(String message) {
        super(message);
    }
}
