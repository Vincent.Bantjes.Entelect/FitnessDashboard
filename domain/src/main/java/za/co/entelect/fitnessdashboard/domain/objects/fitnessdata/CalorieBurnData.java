package za.co.entelect.fitnessdashboard.domain.objects.fitnessdata;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class CalorieBurnData implements Serializable
{
    public Double getCaloriesBurnt() {
        return caloriesBurnt;
    }

    public void setCaloriesBurnt(Double caloriesBurnt) {
        this.caloriesBurnt = caloriesBurnt;
    }

    @NotNull(message = "Calories burnt must be specified.")
    private Double caloriesBurnt;
}
