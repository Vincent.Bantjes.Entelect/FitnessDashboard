package za.co.entelect.fitnessdashboard.domain.objects.fitnessdata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "HEART_RATE_MONITOR_DATA")
@DiscriminatorValue(value = "HEART_RATE_MONITOR_DATA")
public class HeartRateMonitorData extends FitnessData
{
    @Column(name = "HEART_RATE")
    public Integer getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(Integer heartRate) {
        this.heartRate = heartRate;
    }

    @NotNull(message = "The heart rate must be specified.")
    private Integer heartRate;
}