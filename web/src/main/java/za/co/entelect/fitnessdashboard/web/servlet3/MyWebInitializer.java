package za.co.entelect.fitnessdashboard.web.servlet3;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import za.co.entelect.fitnessdashboard.web.config.SpringRootConfig;
import za.co.entelect.fitnessdashboard.web.config.SpringWebSecurityConfig;

public class MyWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer
{
    @Override
    protected Class<?>[] getRootConfigClasses()
    {
        return new Class[] { SpringRootConfig.class };
    }

    @Override
    protected Class<?>[] getServletConfigClasses()
    {
        return new Class[] { SpringWebSecurityConfig.class };
    }

    @Override
    protected String[] getServletMappings()
    {
        return new String[] { "/" };
    }
}
