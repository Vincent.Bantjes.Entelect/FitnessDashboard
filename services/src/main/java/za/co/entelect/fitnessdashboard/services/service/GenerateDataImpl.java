package za.co.entelect.fitnessdashboard.services.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.domain.objects.devices.*;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.*;
import za.co.entelect.fitnessdashboard.domain.objects.goals.Fitness;
import za.co.entelect.fitnessdashboard.domain.objects.goals.WeightLoss;
import za.co.entelect.fitnessdashboard.domain.repositories.devices.*;
import za.co.entelect.fitnessdashboard.domain.repositories.fitnessdata.*;
import za.co.entelect.fitnessdashboard.domain.repositories.goals.FitnessRepository;
import za.co.entelect.fitnessdashboard.domain.repositories.goals.WeightlossRepository;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;

@Configuration
public class GenerateDataImpl extends UtilityService implements GenerateData
{
    @Override
    @Transactional
    public GenerateDevicesResponse generateDevices(GenerateDevicesRequest generateDeviceDataRequest)
            throws PreconditionViolationException
    {
        // Validate input
        validateRequest(generateDeviceDataRequest, logger);

        // Generate devices data
        return generateDevicesForAccount(generateDeviceDataRequest.getAccount());
    }

    private GenerateDevicesResponse generateDevicesForAccount(Account account) throws PreconditionViolationException
    {
        int  amountOfDevices = rand.nextInt(6) + 1;
        int  deviceType = rand.nextInt(6) + 1;
        Boolean bodyFatIndicator = Boolean.FALSE;
        Boolean calorieCounter = Boolean.FALSE;
        Boolean exerciseTracker = Boolean.FALSE;
        Boolean heartRateMonitor = Boolean.FALSE;
        Boolean pedometer = Boolean.FALSE;
        Boolean scale = Boolean.FALSE;
        for (int deviceNumber = 0; deviceNumber <= amountOfDevices; deviceNumber++)
        {
            try
            {
                switch (deviceType)
                {
                    case 1:
                    {
                        if(!bodyFatIndicator)
                        {
                            deviceService.addDeviceToAccount(new DeviceService.AddDeviceToAccountRequest(account,
                                    createBodyFatIndicator()));
                            bodyFatIndicator = Boolean.TRUE;
                        }
                        break;
                    }
                    case 2:
                    {
                        if(!calorieCounter)
                        {
                            deviceService.addDeviceToAccount(new DeviceService.AddDeviceToAccountRequest(account,
                                createCalorieCounter()));
                            calorieCounter = Boolean.TRUE;
                        }
                        break;
                    }
                    case 3:
                    {
                        if(!exerciseTracker)
                        {
                            deviceService.addDeviceToAccount(new DeviceService.AddDeviceToAccountRequest(account,
                                createExerciseTracker()));
                            exerciseTracker = Boolean.TRUE;
                        }
                        break;
                    }
                    case 4:
                    {
                        if(!heartRateMonitor)
                        {
                            deviceService.addDeviceToAccount(new DeviceService.AddDeviceToAccountRequest(account,
                                createHeartRateMonitor()));
                            heartRateMonitor = Boolean.TRUE;
                        }
                        break;
                    }
                    case 5:
                    {
                        if(!pedometer)
                        {
                            deviceService.addDeviceToAccount(new DeviceService.AddDeviceToAccountRequest(account,
                                createPedometer()));
                            pedometer = Boolean.TRUE;
                        }
                        break;
                    }
                    case 6:
                    {
                        if(!scale)
                        {
                            deviceService.addDeviceToAccount(new DeviceService.AddDeviceToAccountRequest(account,
                                createScale()));
                            scale = Boolean.TRUE;
                        }
                        break;
                    }
                }
            }
            catch (DeviceService.DeviceAlreadyExistsException exception)
            {
                logger.info(exception.getMessage());
            }
            deviceType = rand.nextInt(6) + 1;
        }

        return new GenerateDevicesResponse(account);
    }

    @Override
    public GenerateFitnessDataResponse generateFitnessDeviceData(GenerateFitnessDataRequest generateFitnessDataRequest)
            throws PreconditionViolationException
    {
        // Validate input
        validateRequest(generateFitnessDataRequest, logger);

        // Generate devices data
        return generateDeviceDataForAccount(generateFitnessDataRequest.getAccount());
    }

    private GenerateFitnessDataResponse generateDeviceDataForAccount(Account account)
    {
        for (Device device : account.getDevicesLinkedToAccount())
        {
            switch (device.getClass().getName())
            {
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.BodyFatIndicator":
                {
                    device.getFitnessDataLinkedToDevice().add(createBodyFatIndicatorData((BodyFatIndicator)device));
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.CalorieCounter":
                {
                    device.getFitnessDataLinkedToDevice().add(createCalorieCounterData((CalorieCounter)device));
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.ExerciseTracker":
                {
                    device.getFitnessDataLinkedToDevice().add(createExerciseTrackerData((ExerciseTracker)device));
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.HeartRateMonitor":
                {
                    device.getFitnessDataLinkedToDevice().add(createHeartRateMonitorData((HeartRateMonitor)device));
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.Pedometer":
                {
                    device.getFitnessDataLinkedToDevice().add(createPedometerData((Pedometer)device));
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.Scale":
                {
                    device.getFitnessDataLinkedToDevice().add(createScaleData((Scale)device));
                    break;
                }
            }
        }
        return new GenerateFitnessDataResponse(account);
    }

    @Override
    @Transactional
    public GenerateGoalsResponse generateGoals(GenerateGoalsRequest generateGoalsRequest) throws
            PreconditionViolationException
    {
        // Validate input
        validateRequest(generateGoalsRequest, logger);

        // Generate devices data
        return generateGoalsForAccount(generateGoalsRequest.getAccount());
    }

    private GenerateGoalsResponse generateGoalsForAccount(Account account) throws PreconditionViolationException
    {
        BodyFatIndicator bodyFatIndicator = null;
        CalorieCounter calorieCounter = null;
        ExerciseTracker exerciseTracker = null;
        HeartRateMonitor heartRateMonitor = null;
        Pedometer pedometer = null;
        Scale scale = null;
        for (Device device : account.getDevicesLinkedToAccount())
        {
            switch (device.getClass().getName())
            {
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.BodyFatIndicator":
                {
                    bodyFatIndicator = (BodyFatIndicator) device;
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.CalorieCounter":
                {
                    calorieCounter = (CalorieCounter) device;
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.ExerciseTracker":
                {
                    exerciseTracker = (ExerciseTracker) device;
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.HeartRateMonitor":
                {
                    heartRateMonitor = (HeartRateMonitor) device;
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.Pedometer":
                {
                    pedometer = (Pedometer) device;
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.devices.Scale":
                {
                    scale = (Scale) device;
                    break;
                }
            }
        }

        try
        {
            if (exerciseTracker != null || heartRateMonitor != null || pedometer != null)
            {
                goalService.addGoalToAccount(new GoalService.AddGoalToAccountRequest(account, createFitnessGoal(
                        exerciseTracker, heartRateMonitor, pedometer)));
            }
            if (bodyFatIndicator != null || calorieCounter != null || scale != null)
            {
                goalService.addGoalToAccount(new GoalService.AddGoalToAccountRequest(account, createWeightlossGoal(
                        bodyFatIndicator, calorieCounter, scale)));
            }
        }
        catch(GoalService.GoalAlreadyExistsException exception)
        {
            logger.info(exception.getMessage());
        }
        return new GenerateGoalsResponse(account);
    }

    private Fitness createFitnessGoal(ExerciseTracker exerciseTracker, HeartRateMonitor heartRateMonitor,
                                      Pedometer pedometer)
    {
        Fitness fitness = new Fitness();
        if(exerciseTracker != null)
        {
            fitness.setExerciseGoal(createExerciseTrackerData(exerciseTracker));
        }
        if(heartRateMonitor != null)
        {
            fitness.setHeartRateGoal(createHeartRateMonitorData(heartRateMonitor));
        }
        if(pedometer != null)
        {
            fitness.setStepCountGoal(createPedometerData(pedometer));
        }
        return fitnessDao.save(fitness);
    }

    private WeightLoss createWeightlossGoal(BodyFatIndicator bodyFatIndicator, CalorieCounter calorieCounter, Scale scale)
    {
        WeightLoss weightLoss = new WeightLoss();
        if(bodyFatIndicator != null)
        {
            weightLoss.setBodyFatIndicatorGoal(createBodyFatIndicatorData(bodyFatIndicator));
        }
        if(calorieCounter != null)
        {
            weightLoss.setCalorieCounterGoal(createCalorieCounterData(calorieCounter));
        }
        if(scale != null)
        {
            weightLoss.setWeightGoal(createScaleData(scale));
        }
        return weightLossDao.save(weightLoss);
    }

    private BodyFatIndicator createBodyFatIndicator()
    {
        return bodyFatIndicatorDao.save(new BodyFatIndicator());
    }

    private CalorieCounter createCalorieCounter()
    {
        return calorieCounterDao.save(new CalorieCounter());
    }

    private ExerciseTracker createExerciseTracker()
    {
        return exerciseTrackerDao.save(new ExerciseTracker());
    }

    private HeartRateMonitor createHeartRateMonitor()
    {
        return heartRateMonitorDao.save(new HeartRateMonitor());
    }

    private Pedometer createPedometer()
    {
        return pedometerDao.save(new Pedometer());
    }

    private Scale createScale()
    {
        return scaleDao.save(new Scale());
    }

    private BodyFatIndicatorData createBodyFatIndicatorData(BodyFatIndicator device)
    {
        BodyFatIndicatorData deviceData = new BodyFatIndicatorData();
        deviceData.setDevice(device);
        deviceData.setCaptureDate(LocalDateTime.now());
        deviceData.setBodyFatPercentage(Integer.valueOf(rand.nextInt(100) + 1));
        return bodyFatIndicatorDataDao.save(deviceData);
    }

    private CalorieCounterData createCalorieCounterData(CalorieCounter device)
    {
        CalorieCounterData deviceData = new CalorieCounterData();
        deviceData.setDevice(device);
        deviceData.setCaptureDate(LocalDateTime.now());
        deviceData.setSubstanceConsumed("Food" + Integer.valueOf(rand.nextInt(6) + 1));
        deviceData.setCalorieCount(Integer.valueOf(rand.nextInt(500) + 1));
        return calorieCounterDataDao.save(deviceData);
    }

    private ExerciseTrackerData createExerciseTrackerData(ExerciseTracker device)
    {
        ExerciseTrackerData deviceData = new ExerciseTrackerData();
        deviceData.setDevice(device);
        deviceData.setCaptureDate(LocalDateTime.now());
        deviceData.setExerciseType(ExerciseTrackerData.EXERCISE_TYPE.RUN);
        deviceData.setStartDate(LocalDateTime.now().minus(Integer.valueOf(rand.nextInt(24) + 1).intValue(),
                ChronoUnit.HOURS));
        deviceData.setEndDate(LocalDateTime.now());
        return exerciseTrackerDataDao.save(deviceData);
    }

    private HeartRateMonitorData createHeartRateMonitorData(HeartRateMonitor device)
    {
        HeartRateMonitorData deviceData = new HeartRateMonitorData();
        deviceData.setDevice(device);
        deviceData.setCaptureDate(LocalDateTime.now());
        deviceData.setHeartRate(Integer.valueOf(rand.nextInt(160) + 1));
        return heartRateMonitorDataDao.save(deviceData);
    }

    private PedometerData createPedometerData(Pedometer device)
    {
        PedometerData deviceData = new PedometerData();
        deviceData.setDevice(device);
        deviceData.setCaptureDate(LocalDateTime.now());
        deviceData.setStepCount(Integer.valueOf(rand.nextInt(10000) + 1));
        return pedometerDataDao.save(deviceData);
    }

    private ScaleData createScaleData(Scale device)
    {
        ScaleData deviceData = new ScaleData();
        deviceData.setDevice(device);
        deviceData.setCaptureDate(LocalDateTime.now());
        deviceData.setWeight(Integer.valueOf(rand.nextInt(120) + 1));
        return scaleDataDao.save(deviceData);
    }

    @Autowired
    private BodyFatIndicatorRepository bodyFatIndicatorDao;

    @Autowired
    private CalorieCounterRepository calorieCounterDao;

    @Autowired
    private ExerciseTrackerRepository exerciseTrackerDao;

    @Autowired
    private HeartRateMonitorRepository heartRateMonitorDao;

    @Autowired
    private PedometerRepository pedometerDao;

    @Autowired
    private ScaleRepository scaleDao;

    @Autowired
    private BodyFatIndicatorDataRepository bodyFatIndicatorDataDao;

    @Autowired
    private CalorieCounterDataRepository calorieCounterDataDao;

    @Autowired
    private ExerciseTrackerDataRepository exerciseTrackerDataDao;

    @Autowired
    private HeartRateMonitorDataRepository heartRateMonitorDataDao;

    @Autowired
    private PedometerDataRepository pedometerDataDao;

    @Autowired
    private ScaleDataRepository scaleDataDao;

    @Autowired
    private FitnessRepository fitnessDao;

    @Autowired
    private WeightlossRepository weightLossDao;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private GoalService goalService;

    private static Random rand = new Random();

    private Logger logger = LoggerFactory.getLogger(GenerateDataImpl.class);
}