package za.co.entelect.fitnessdashboard.domain.objects.goals;

import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.ExerciseTrackerData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.HeartRateMonitorData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.PedometerData;

import javax.persistence.*;
import javax.validation.Valid;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "FITNESS")
@DiscriminatorValue(value = "FITNESS")
public class Fitness extends Goal
{
    @ManyToOne
    @JoinColumn(name = "EXERCISE_GOAL")
    public ExerciseTrackerData getExerciseGoal() {
        return exerciseGoal;
    }

    public void setExerciseGoal(ExerciseTrackerData exerciseGoal) {
        this.exerciseGoal = exerciseGoal;
    }

    @ManyToOne
    @JoinColumn(name = "HEART_RATE_MONITOR_GOAL")
    public HeartRateMonitorData getHeartRateGoal() {
        return heartRateGoal;
    }

    public void setHeartRateGoal(HeartRateMonitorData heartRateGoal) {
        this.heartRateGoal = heartRateGoal;
    }

    @ManyToOne
    @JoinColumn(name = "STEP_COUNT_GOAL")
    public PedometerData getStepCountGoal() {
        return stepCountGoal;
    }

    public void setStepCountGoal(PedometerData stepCountGoal) {
        this.stepCountGoal = stepCountGoal;
    }

    @Valid
    private PedometerData stepCountGoal;

    @Valid
    private ExerciseTrackerData exerciseGoal;

    @Valid
    private HeartRateMonitorData heartRateGoal;
}
