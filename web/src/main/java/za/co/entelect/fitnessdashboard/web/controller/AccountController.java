package za.co.entelect.fitnessdashboard.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.domain.objects.account.Gender;
import za.co.entelect.fitnessdashboard.domain.objects.goals.Fitness;
import za.co.entelect.fitnessdashboard.domain.objects.goals.Goal;
import za.co.entelect.fitnessdashboard.domain.objects.security.User;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;
import za.co.entelect.fitnessdashboard.services.service.AccountService;
import za.co.entelect.fitnessdashboard.web.models.AccountForm;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class AccountController
{
    @GetMapping("/account")
    public String processAccount(Authentication authentication, Model model, AccountForm accountForm, BindingResult
            result)
    {
        logger.info("View Account processing...");
        try
        {
            AccountService.RetrieveAccountByUsernameResponse retrieveAccountByUsernameResponse =
                    accountService.retrieveAccountByUsername(new AccountService.
                            RetrieveAccountByUsernameRequest(authentication.getName()));

            model.addAttribute("account", retrieveAccountByUsernameResponse.getAccount());


            for (Goal goal: retrieveAccountByUsernameResponse.getAccount().getGoalsLinkedToAccount())
            {
                if(goal instanceof Fitness)
                {

                }
                else
                {

                }
            }
            model.addAttribute("fitnessgoals", null);
            model.addAttribute("weightlossgoals", null);
            return "account";
        }
        catch(PreconditionViolationException | AccountService.AccountDoesNotExistException exception)
        {
            logger.error("{}", exception.getMessage());
            model.addAttribute("message", exception.getMessage());
            return "/";
        }
    }

    @GetMapping("/account/{id}")
    public String processAccount(Model model, @PathVariable("id") Long id, @ModelAttribute("accountForm")
            AccountForm accountForm, BindingResult result)
    {
        logger.info("View Account by id processing...");
        try
        {
            AccountService.RetrieveAccountByIdResponse retrieveAccountByUsernameResponse =
                    accountService.retrieveAccountById(new AccountService.
                            RetrieveAccountByIdRequest(id));

            model.addAttribute("account", retrieveAccountByUsernameResponse.getAccount());
            return "account";
        }
        catch(PreconditionViolationException | AccountService.AccountDoesNotExistException exception)
        {
            logger.error("{}", exception.getMessage());
            model.addAttribute("message", exception.getMessage());
            return "home";
        }
    }

    @GetMapping("/admin")
    public String processAdmin(Authentication authentication, Model model)
    {
        logger.info("View Admin processing...");
        try
        {
            AccountService.RetrieveAccountByUsernameResponse retrieveAccountByUsernameResponse =
                    accountService.retrieveAccountByUsername(new AccountService.
                            RetrieveAccountByUsernameRequest(authentication.getName()));

            model.addAttribute("account", retrieveAccountByUsernameResponse.getAccount());

            AccountService.RetrieveAllAccountsResponse retrieveAllAccounts =
                    accountService.retrieveAllAccounts(new AccountService.RetrieveAllAccountsRequest());

            List<AccountForm> accountForms = new ArrayList<>();

            for (Account account : retrieveAllAccounts.getAccounts())
            {
                AccountForm accountForm = new AccountForm();
                accountForm.setUsername(account.getUser().getUsername());
                accountForm.setGender(account.getGender());
                accountForm.setAge(account.getAge().toString());
                accountForm.setId(account.getSystemId());
                accountForm.setRoles(accountService.retrieveRoles(new AccountService.RetrieveRolesRequest(account))
                        .getRoles());
                accountForms.add(accountForm);
            }

            model.addAttribute("listAccounts", accountForms);

            return "admin";
        }
        catch(PreconditionViolationException | AccountService.AccountDoesNotExistException exception)
        {
            logger.error("{}", exception.getMessage());
            model.addAttribute("message", exception.getMessage());
            return "/";
        }
    }

    @PostMapping("/account/update")
    public final String processUpdateAccount(Authentication authentication, @ModelAttribute("accountForm")
            AccountForm accountForm, BindingResult result, Model model)
    {
        logger.info("Update processing...");

        accountFormValidator.validate(accountForm, result);
        if (result.hasErrors())
        {
            logger.error("{}", result.toString());
            model.addAttribute(BindingResult.class.getName() + ".accountForm", result);
            return "account";
        }

        try
        {
            AccountService.RetrieveAccountByUsernameResponse retrieveAccountByUsernameResponse =
                    accountService.retrieveAccountByUsername(new AccountService.
                            RetrieveAccountByUsernameRequest(authentication.getName()));

            User user = new User();
            user.setUsername(accountForm.getUsername());
            user.setPassword(passwordEncoder.encode(accountForm.getPassword()));
            user.setSignedUp(LocalDateTime.now());

            Account account = new Account();
            account.setGender(accountForm.getGender());
            account.setUser(user);
            account.setAge(Integer.valueOf(accountForm.getAge()));

            AccountService.UpdateResponse response = accountService.update(new AccountService.
                    UpdateRequest(retrieveAccountByUsernameResponse.getAccount(), account));
            if (response.getAccount() != null)
            {
                model.addAttribute("account", response.getAccount());
                return "account";
            }
            else
            {
                model.addAttribute("message", "Something went wrong. Please try again.");
                return "redirect:/account";
            }
        }
        catch(PreconditionViolationException | AccountService.AccountAlreadyExistsException |
                AccountService.AccountDoesNotExistException exception)
        {
            logger.error("{}", exception.getMessage());
            model.addAttribute("message", "Something went wrong. Please try again." + exception.getMessage());
            return "redirect:/account";
        }
    }

    @Autowired
    private AccountFormValidator accountFormValidator;

    @Autowired
    private AccountService accountService;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private final Logger logger = LoggerFactory.getLogger(AccountController.class);
}