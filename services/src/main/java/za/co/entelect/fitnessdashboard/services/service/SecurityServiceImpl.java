package za.co.entelect.fitnessdashboard.services.service;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.domain.objects.security.LoginEvent;
import za.co.entelect.fitnessdashboard.domain.objects.security.User;
import za.co.entelect.fitnessdashboard.domain.repositories.account.AccountRepository;
import za.co.entelect.fitnessdashboard.domain.repositories.security.LoginEventRepository;
import za.co.entelect.fitnessdashboard.domain.repositories.security.UserRepository;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import java.time.LocalDateTime;

@Configuration
public class SecurityServiceImpl extends UtilityService implements SecurityService
{
    @Override
    @Transactional
    public Boolean login(String username, String password)
    {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new
                UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
        usernamePasswordAuthenticationToken.setDetails(userDetails.getUsername());

        Authentication authentication = authenticationManager.authenticate(
                usernamePasswordAuthenticationToken);

        if (usernamePasswordAuthenticationToken.isAuthenticated()) {
            SecurityContextHolder.getContext().setAuthentication(authentication);
            createLoginEvent(getUserIfExistsByUserName(username));
            logger.debug("Auto login {} successfully!", username);
            return Boolean.TRUE;
        }
        else
        {
            return Boolean.FALSE;
        }
    }

    @Override
    public String findLoggedInUsername()
    {
        Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
        if (userDetails instanceof UserDetails)
        {
            return ((UserDetails)userDetails).getUsername();
        }

        return null;
    }

    @Override
    @Transactional
    public LoginResponse login(LoginRequest loginRequest) throws PreconditionViolationException
    {
        // Validate input
        validateRequest(loginRequest, logger);

        // Calculate calorie count
        if(checkIfAccountExists(new CheckIfAccountExistsRequest(loginRequest.getUsername())).getCheckResult())
        {
            return new LoginResponse(validateLogin(getUserIfExistsByUserName(loginRequest.getUsername()),
                    loginRequest.getPassword()));
        }
        else
        {
            return new LoginResponse(Boolean.FALSE);
        }
    }

    @Override
    public CheckIfAccountExistsResponse checkIfAccountExists(CheckIfAccountExistsRequest
                                                                         checkIfAccountExistsRequest)
            throws PreconditionViolationException
    {
        // Validate input
        validateRequest(checkIfAccountExistsRequest, logger);

        if(Strings.isNullOrEmpty(checkIfAccountExistsRequest.getUsername()) && checkIfAccountExistsRequest.
                getId() == null)
        {
            throw new PreconditionViolationException("At least one of the input values must be specified.");
        }

        if(checkIfAccountExistsRequest.getId() != null)
        {
            Account account = getAccountIfExistsById(checkIfAccountExistsRequest.getId());
            if(account != null)
            {
                return new CheckIfAccountExistsResponse(Boolean.TRUE);
            }
            else
            {
                logger.error("Could not find account by id {}", checkIfAccountExistsRequest.getId());
            }
        }
        else if(!Strings.isNullOrEmpty(checkIfAccountExistsRequest.getUsername()))
        {
            User user = getUserIfExistsByUserName(checkIfAccountExistsRequest.getUsername());
            if(user != null)
            {
                return new CheckIfAccountExistsResponse(Boolean.TRUE);
            }
            else
            {
                logger.error("Could not find account by username {}", checkIfAccountExistsRequest.getUsername());
            }
        }

        return new CheckIfAccountExistsResponse(Boolean.FALSE);
    }

    private User getUserIfExistsByUserName(String username)
    {
        return userDao.findByUsername(username);
    }

    private Account getAccountIfExistsById(Long id)
    {
        return accountDao.findOne(id);
    }

    private Boolean validateLogin(User user, String password)
    {
        return login(user.getUsername(), password);
    }

    private LoginEvent createLoginEvent(User user)
    {
        LoginEvent loginEvent = new LoginEvent();
        loginEvent.setUser(user);
        LocalDateTime time = LocalDateTime.now();
        loginEvent.setSignInDate(time);
        return loginEventDao.save(loginEvent);
    }

    private Logger logger = LoggerFactory.getLogger(SecurityServiceImpl.class);

    @Autowired
    private LoginEventRepository loginEventDao;

    @Autowired
    private AccountRepository accountDao;

    @Autowired
    private UserRepository userDao;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationManager authenticationManager;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
}