package za.co.entelect.fitnessdashboard.services.service;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.stereotype.Service;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.LinkedHashSet;
import java.util.Set;

@Service
public interface AccountService
{
    RegisterResponse register(RegisterRequest registerRequest) throws PreconditionViolationException,
            AccountAlreadyExistsException;

    class RegisterRequest
    {
        public RegisterRequest(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that needs to be registered must be specified.")
        private Account account;
    }

    class RegisterResponse
    {
        public RegisterResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that has been registered must be specified.")
        private Account account;
    }

    UpdateResponse update(UpdateRequest updateRequest) throws PreconditionViolationException,
            AccountAlreadyExistsException, AccountDoesNotExistException;

    class UpdateRequest
    {
        public UpdateRequest(Account currentAccount, Account newAccountWithDetails)
        {
            this.currentAccount = currentAccount;
            this.newAccountWithDetails = newAccountWithDetails;
        }

        public Account getCurrentAccount()
        {
            return currentAccount;
        }

        public void setCurrentAccount(Account currentAccount)
        {
            this.currentAccount = currentAccount;
        }

        public Account getNewAccountWithDetails()
        {
            return newAccountWithDetails;
        }

        public void setNewAccountWithDetails(Account newAccountWithDetails)
        {
            this.newAccountWithDetails = newAccountWithDetails;
        }

        @Valid
        @NotNull(message = "An account that needs to be updated must be specified.")
        private Account currentAccount;

        @Valid
        @NotNull(message = "An account that has the new details must be specified.")
        private Account newAccountWithDetails;
    }

    class UpdateResponse
    {
        public UpdateResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that has been updated must be specified.")
        private Account account;
    }

    RetrieveAccountByUsernameResponse retrieveAccountByUsername(RetrieveAccountByUsernameRequest
            retrieveAccountByUsernameRequest) throws PreconditionViolationException,
            AccountDoesNotExistException;

    class RetrieveAccountByUsernameRequest
    {
        public RetrieveAccountByUsernameRequest(String username)
        {
            this.username = username;
        }

        public String getUsername()
        {
            return username;
        }

        public void setUsername(String username)
        {
            this.username = username;
        }

        @NotNull(message = "A username must be specified.")
        private String username;
    }

    class RetrieveAccountByUsernameResponse
    {
        public RetrieveAccountByUsernameResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that has been registered must be specified.")
        private Account account;
    }

    RetrieveAccountByIdResponse retrieveAccountById(RetrieveAccountByIdRequest
            retrieveAccountByUsernameRequest) throws PreconditionViolationException,
            AccountDoesNotExistException;

    class RetrieveAccountByIdRequest
    {
        public RetrieveAccountByIdRequest(Long id)
        {
            this.id = id;
        }

        public Long getId()
        {
            return id;
        }

        public void setId(Long id)
        {
            this.id = id;
        }

        @NotNull(message = "An id must be specified.")
        private Long id;
    }

    class RetrieveAccountByIdResponse
    {
        public RetrieveAccountByIdResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that has been retrieved must be specified.")
        private Account account;
    }

    RetrieveAllAccountsResponse retrieveAllAccounts(RetrieveAllAccountsRequest retrieveAllAccountsRequest) throws
            PreconditionViolationException;

    class RetrieveAllAccountsRequest
    {
        public RetrieveAllAccountsRequest() { }
    }

    class RetrieveAllAccountsResponse
    {
        public RetrieveAllAccountsResponse() { }

        public Set<Account> getAccounts()
        {
            return accounts;
        }

        public void setAccounts(Set<Account> accounts)
        {
            this.accounts = accounts;
        }

        @Valid
        @NotNull(message = "An accounts list must be initialized.")
        private Set<Account> accounts = new LinkedHashSet<>();
    }

    RetrieveRolesResponse retrieveRoles(RetrieveRolesRequest retrieveRolesRequest) throws PreconditionViolationException;

    class RetrieveRolesRequest
    {
        public RetrieveRolesRequest(Account account)
        {
            this.account = account;
        }

        public Account getAccount()
        {
            return account;
        }

        public void setAccount(Account account)
        {
            this.account = account;
        }

        @Valid
        @NotNull(message = "The account for which the roles must be retrieved must be specified.")
        private Account account;
    }

    class RetrieveRolesResponse
    {
        public RetrieveRolesResponse() { }

        public String getRoles()
        {
            return roles;
        }

        public void setRoles(String roles)
        {
            this.roles = roles;
        }

        @NotEmpty(message = "The roles for the account must be specified.")
        private String roles;
    }

    class AccountAlreadyExistsException extends Exception
    {
        public AccountAlreadyExistsException() { }

        public AccountAlreadyExistsException(String message) {
            super(message);
        }
    }

    class AccountDoesNotExistException extends Exception
    {
        public AccountDoesNotExistException() { }

        public AccountDoesNotExistException(String message) {
            super(message);
        }
    }
}
