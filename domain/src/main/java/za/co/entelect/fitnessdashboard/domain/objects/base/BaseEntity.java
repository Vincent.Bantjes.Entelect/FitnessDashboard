package za.co.entelect.fitnessdashboard.domain.objects.base;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
@Access(AccessType.PROPERTY)
public abstract class BaseEntity implements Serializable
{
    public BaseEntity() {}

    public BaseEntity(Long systemId)
    {
        this.setSystemId(systemId);
    }

    @Id
    @SequenceGenerator(name = "SEQ_BASE_ENTITY", sequenceName = "SEQ_BASE_ENTITY", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_BASE_ENTITY")
    @Column(name = "SYSTEM_ID", nullable = false, updatable = false, insertable = true)
    public Long getSystemId()
    {
        return systemId;
    }

    void setSystemId(Long systemId)
    {
        this.systemId = systemId;
    }

    @Version
    @Column(name = "VERSION", nullable = false)
    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version)
    {
        this.version = version;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((systemId == null) ? 0 : systemId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        BaseEntity other = (BaseEntity) obj;
        if (systemId == null)
        {
            if (other.systemId != null)
            {
                return false;
            }
        }
        else if (!systemId.equals(other.systemId))
        {
            return false;
        }
        return true;
    }

    private Long systemId;

    private Long version;
}