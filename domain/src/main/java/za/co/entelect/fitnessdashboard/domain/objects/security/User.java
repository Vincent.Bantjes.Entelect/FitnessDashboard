package za.co.entelect.fitnessdashboard.domain.objects.security;

import za.co.entelect.fitnessdashboard.domain.objects.base.BaseEntity;
import za.co.entelect.fitnessdashboard.domain.objects.converter.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "USER")
public class User extends BaseEntity
{
    public User() { }

    public User(User user)
    {
        super(user.getSystemId());
        this.username = user.username;
        this.password = user.password;
        this.signedUp = user.getSignedUp();
    }

    @Column(name = "USERNAME")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "PASSWORD")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Convert(converter = LocalDateTimeAttributeConverter.class)
    @Column(name = "SIGNED_UP")
    public LocalDateTime getSignedUp() {
        return signedUp;
    }

    public void setSignedUp(LocalDateTime signedUp) {
        this.signedUp = signedUp;
    }

    @NotNull(message = "A username must be specified.")
    @Size(min = 1, message = "A username must be specified.")
    protected String username;

    @NotNull(message = "A password must be specified.")
    @Size(min = 1, message = "A password must be specified.")
    protected String password;

    @NotNull(message = "A signed up date must be specified.")
    protected LocalDateTime signedUp;
}