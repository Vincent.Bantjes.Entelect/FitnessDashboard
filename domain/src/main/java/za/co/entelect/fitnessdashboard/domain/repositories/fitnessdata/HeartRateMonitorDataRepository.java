package za.co.entelect.fitnessdashboard.domain.repositories.fitnessdata;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.HeartRateMonitorData;

@Repository
public interface HeartRateMonitorDataRepository extends CrudRepository<HeartRateMonitorData, Long> { }
