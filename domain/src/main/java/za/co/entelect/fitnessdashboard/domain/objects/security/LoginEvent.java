package za.co.entelect.fitnessdashboard.domain.objects.security;

import za.co.entelect.fitnessdashboard.domain.objects.base.BaseEntity;
import za.co.entelect.fitnessdashboard.domain.objects.converter.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "LOGIN_EVENT")
public class LoginEvent extends BaseEntity
{
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    @Column(name = "SIGN_IN_DATE")
    public LocalDateTime getSignInDate() {
        return signInDate;
    }

    public void setSignInDate(LocalDateTime signInDate) {
        this.signInDate = signInDate;
    }

    @ManyToOne
    @JoinColumn(name = "USER")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @NotNull(message = "The sign in date must be specified.")
    private LocalDateTime signInDate;

    @Valid
    @NotNull(message = "The user for the sign in must be specified.")
    private User user;
}
