package za.co.entelect.fitnessdashboard.domain.objects.goals;

import za.co.entelect.fitnessdashboard.domain.objects.base.BaseEntity;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.FitnessData;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "GOAL")
@DiscriminatorColumn(name = "GOAL", discriminatorType = DiscriminatorType.STRING, length = 80)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Goal extends BaseEntity
{
    @OneToMany
    @JoinColumn(name = "FITNESS_DATA")
    public List<FitnessData> getFitessDataLinkedToGoal() {
        return fitessDataLinkedToGoal;
    }

    public void setFitessDataLinkedToGoal(List<FitnessData> fitessDataLinkedToGoal) {
        this.fitessDataLinkedToGoal = fitessDataLinkedToGoal;
    }

    @Valid
    @NotNull(message = "The fitness data must be initialized.")
    private List<FitnessData> fitessDataLinkedToGoal = new ArrayList();
}
