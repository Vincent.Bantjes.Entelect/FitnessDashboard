package za.co.entelect.fitnessdashboard.services.service;

import org.springframework.stereotype.Service;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.CalorieBurnData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.ExerciseTrackerData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.HeartRateMonitorData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.ScaleData;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Service
public interface CalorieCalculator
{
    CalorieBurnData calculateCaloriesBurnt(CalculateCaloriesBurntRequest calculateCaloriesBurntRequest)
            throws PreconditionViolationException;

    class CalculateCaloriesBurntRequest
    {
        public ExerciseTrackerData getExerciseTrackerData() {
            return exerciseTrackerData;
        }

        public void setExerciseTrackerData(ExerciseTrackerData exerciseTrackerData) {
            this.exerciseTrackerData = exerciseTrackerData;
        }

        public HeartRateMonitorData getHeartRateMonitorData() {
            return heartRateMonitorData;
        }

        public void setHeartRateMonitorData(HeartRateMonitorData heartRateMonitorData) {
            this.heartRateMonitorData = heartRateMonitorData;
        }

        public ScaleData getScaleData() {
            return scaleData;
        }

        public void setScaleData(ScaleData scaleData) {
            this.scaleData = scaleData;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "The exercise must be specified.")
        private ExerciseTrackerData exerciseTrackerData;

        @Valid
        @NotNull(message = "The heart rate must be specified.")
        private HeartRateMonitorData heartRateMonitorData;

        @Valid
        @NotNull(message = "The scale data with the weight must be specified.")
        private ScaleData scaleData;

        @Valid
        @NotNull(message = "An account for whom the calories need to be calculated must be specified.")
        private Account account;
    }
}
