package za.co.entelect.fitnessdashboard.domain.objects.fitnessdata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "BODY_FAT_INDICATOR_DATA")
@DiscriminatorValue(value = "BODY_FAT_INDICATOR_DATA")
public class BodyFatIndicatorData extends FitnessData
{
    @Column(name = "BODY_FAT_PERCENTAGE")
    public Integer getBodyFatPercentage() {
        return bodyFatPercentage;
    }

    public void setBodyFatPercentage(Integer bodyFatPercentage) {
        this.bodyFatPercentage = bodyFatPercentage;
    }

    @NotNull(message = "The body fat percentage must be specified.")
    private Integer bodyFatPercentage;
}
