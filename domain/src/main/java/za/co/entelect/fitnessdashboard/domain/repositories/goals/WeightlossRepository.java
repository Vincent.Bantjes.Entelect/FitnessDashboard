package za.co.entelect.fitnessdashboard.domain.repositories.goals;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.goals.WeightLoss;

@Repository
public interface WeightlossRepository extends CrudRepository<WeightLoss, Long> { }