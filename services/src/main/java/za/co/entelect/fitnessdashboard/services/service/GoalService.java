package za.co.entelect.fitnessdashboard.services.service;

import org.springframework.stereotype.Service;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.domain.objects.goals.Goal;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Service
public interface GoalService
{
    AddGoalResponse addGoal(AddGoalRequest addGoalRequest)
            throws PreconditionViolationException, GoalAlreadyExistsException, GoalNotSupportedException;

    class AddGoalRequest
    {
        public AddGoalRequest(Goal goal)
        {
            this.goal = goal;
        }

        public Goal getGoal()
        {
            return goal;
        }

        public void setGoal(Goal goal)
        {
            this.goal = goal;
        }

        @Valid
        @NotNull(message = "A goal that needs to be added must be specified.")
        private Goal goal;
    }

    class AddGoalResponse
    {
        public AddGoalResponse(Goal goal)
        {
            this.goal = goal;
        }

        public Goal getGoal()
        {
            return goal;
        }

        public void setGoal(Goal goal)
        {
            this.goal = goal;
        }

        @Valid
        @NotNull(message = "A goal that has been added to must be specified.")
        private Goal goal;
    }

    AddGoalToAccountResponse addGoalToAccount(AddGoalToAccountRequest addGoalToAccountRequest)
            throws PreconditionViolationException, GoalAlreadyExistsException;

    class AddGoalToAccountRequest
    {
        public AddGoalToAccountRequest(Account account, Goal goal)
        {
            this.account = account;
            this.goal = goal;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        public Goal getGoal()
        {
            return goal;
        }

        public void setGoal(Goal goal)
        {
            this.goal = goal;
        }

        @Valid
        @NotNull(message = "An account that the goal is to be added to must be specified.")
        private Account account;

        @Valid
        @NotNull(message = "A goal that needs to be added must be specified.")
        private Goal goal;
    }

    class AddGoalToAccountResponse
    {
        public AddGoalToAccountResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that goal has been added to must be specified.")
        private Account account;
    }

    RemoveGoalFromAccountResponse removeGoalFromAccount(RemoveGoalFromAccountRequest removeGoalFromAccountRequest)
            throws PreconditionViolationException,
            GoalDoesNotExistsException;

    class RemoveGoalFromAccountRequest
    {
        public RemoveGoalFromAccountRequest(Account account, Goal goal)
        {
            this.account = account;
            this.goal = goal;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        public Goal getGoal()
        {
            return goal;
        }

        public void setGoal(Goal goal)
        {
            this.goal = goal;
        }

        @Valid
        @NotNull(message = "An account that the goal is to be removed from must be specified.")
        private Account account;

        @Valid
        @NotNull(message = "A goal that needs to be removed must be specified.")
        private Goal goal;
    }

    class RemoveGoalFromAccountResponse
    {
        public RemoveGoalFromAccountResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that the goal has been removed from must be specified.")
        private Account account;
    }

    class GoalAlreadyExistsException extends Exception
    {
        public GoalAlreadyExistsException() { }

        public GoalAlreadyExistsException(String message) {
            super(message);
        }
    }

    class GoalDoesNotExistsException extends Exception
    {
        public GoalDoesNotExistsException() { }

        public GoalDoesNotExistsException(String message) {
            super(message);
        }
    }

    class GoalNotSupportedException extends Exception
    {
        public GoalNotSupportedException() { }

        public GoalNotSupportedException(String message) {
            super(message);
        }
    }
}