package za.co.entelect.fitnessdashboard.domain.objects.security;

import za.co.entelect.fitnessdashboard.domain.objects.base.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "USER_ROLE")
public class UserRole extends BaseEntity
{
    @Column(name="USER_ID")
    public Long getUserid()
    {
        return userid;
    }

    public void setUserid(Long userid)
    {
        this.userid = userid;
    }

    @Column(name="ROLE")
    public String getRole()
    {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @NotNull(message = "A user id must be specified.")
    private Long userid;

    @NotNull(message = "A role must be specified.")
    private String role;
}