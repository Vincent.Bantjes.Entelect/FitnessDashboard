package za.co.entelect.fitnessdashboard.services.service;

import org.springframework.stereotype.Service;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.domain.objects.devices.Device;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Service
public interface DeviceService
{
    AddDeviceToAccountResponse addDeviceToAccount(AddDeviceToAccountRequest addDeviceToAccountRequest)
            throws PreconditionViolationException, DeviceAlreadyExistsException;

    class AddDeviceToAccountRequest
    {
        public AddDeviceToAccountRequest(Account account, Device device)
        {
            this.account = account;
            this.device = device;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        public Device getDevice()
        {
            return device;
        }

        public void setDevice(Device device)
        {
            this.device = device;
        }

        @Valid
        @NotNull(message = "An account that device is to be added to must be specified.")
        private Account account;

        @Valid
        @NotNull(message = "A device that needs to be added must be specified.")
        private Device device;
    }

    class AddDeviceToAccountResponse
    {
        public AddDeviceToAccountResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that device has been added to must be specified.")
        private Account account;
    }

    RemoveDeviceFromAccountResponse removeDeviceFromAccount(RemoveDeviceFromAccountRequest
            removeDeviceFromAccountRequest) throws PreconditionViolationException, DeviceDoesNotExistsException;

    class RemoveDeviceFromAccountRequest
    {
        public RemoveDeviceFromAccountRequest(Account account, Device device)
        {
            this.account = account;
            this.device = device;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        public Device getDevice()
        {
            return device;
        }

        public void setDevice(Device device)
        {
            this.device = device;
        }

        @Valid
        @NotNull(message = "An account that device is to be removed from must be specified.")
        private Account account;

        @Valid
        @NotNull(message = "A device that needs to be removed must be specified.")
        private Device device;
    }

    class RemoveDeviceFromAccountResponse
    {
        public RemoveDeviceFromAccountResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that device has been removed from must be specified.")
        private Account account;
    }

    class DeviceAlreadyExistsException extends Exception
    {
        public DeviceAlreadyExistsException() {
        }

        public DeviceAlreadyExistsException(String message) {
            super(message);
        }
    }

    class DeviceDoesNotExistsException extends Exception
    {
        public DeviceDoesNotExistsException() {
        }

        public DeviceDoesNotExistsException(String message) {
            super(message);
        }
    }
}
