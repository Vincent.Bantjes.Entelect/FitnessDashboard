package za.co.entelect.fitnessdashboard.domain.objects.fitnessdata;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "CALORIE_COUNTER_DATA")
@DiscriminatorValue(value = "CALORIE_COUNTER_DATA")
public class CalorieCounterData extends FitnessData
{
    @Column(name = "SUBSTANCE_CONSUMED")
    public String getSubstanceConsumed() {
        return substanceConsumed;
    }

    public void setSubstanceConsumed(String substanceConsumed) {
        this.substanceConsumed = substanceConsumed;
    }

    @Column(name = "CALORIE_COUNT")
    public Integer getCalorieCount() {
        return calorieCount;
    }

    public void setCalorieCount(Integer calorieCount) {
        this.calorieCount = calorieCount;
    }

    @NotNull(message = "The substance consumed must be specified.")
    private String substanceConsumed;

    @NotNull(message = "The calorie count must be specified.")
    private Integer calorieCount;
}
