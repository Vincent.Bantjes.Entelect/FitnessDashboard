package za.co.entelect.fitnessdashboard.services.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import za.co.entelect.fitnessdashboard.domain.config.JPAConfig;

@Configuration
@Import({ JPAConfig.class })
@ComponentScan
(
        basePackages = "za.co.entelect.fitnessdashboard.services",
        includeFilters = {@ComponentScan.Filter(Component.class), @ComponentScan.Filter(Service.class)}
)
public class ServiceConfig { }