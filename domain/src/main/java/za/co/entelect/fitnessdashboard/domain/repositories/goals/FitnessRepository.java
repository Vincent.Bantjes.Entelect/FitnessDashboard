package za.co.entelect.fitnessdashboard.domain.repositories.goals;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import za.co.entelect.fitnessdashboard.domain.objects.goals.Fitness;

@Repository
public interface FitnessRepository extends CrudRepository<Fitness, Long> { }