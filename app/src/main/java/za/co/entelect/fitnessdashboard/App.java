package za.co.entelect.fitnessdashboard;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;

import za.co.entelect.fitnessdashboard.domain.objects.account.Gender;
import za.co.entelect.fitnessdashboard.services.service.GenerateData;
import za.co.entelect.fitnessdashboard.services.service.GenerateData.GenerateDevicesRequest;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.domain.objects.devices.Device;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.FitnessData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.BodyFatIndicatorData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.CalorieCounterData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.ExerciseTrackerData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.HeartRateMonitorData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.PedometerData;
import za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.ScaleData;
import za.co.entelect.fitnessdashboard.domain.objects.security.User;
import za.co.entelect.fitnessdashboard.domain.objects.goals.Goal;
import za.co.entelect.fitnessdashboard.domain.objects.goals.Fitness;
import za.co.entelect.fitnessdashboard.domain.objects.goals.WeightLoss;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;
import za.co.entelect.fitnessdashboard.services.service.AccountService;
import za.co.entelect.fitnessdashboard.services.service.SecurityService;

@SpringBootApplication
public class App implements CommandLineRunner
{
    public static void main(String[] args) throws Exception
    {
        SpringApplication app = new SpringApplication(App.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception
    {
        String username = "U";
        String password = "P";
        Integer age = Integer.valueOf(1);
        Integer gender = Integer.valueOf(1);

        Boolean doneFirstChoice = false;
        int firstChoice = 0;
        int secondChoice = 1;

        BufferedReader bufferedReader = null;
        try
        {
            bufferedReader = new BufferedReader(new InputStreamReader(System.in));

            while (true)
            {
                if(!doneFirstChoice)
                {
                    outputInfoMessage("Please enter 1 to register or 2 to login. Quit to exit.");
                    String input = bufferedReader.readLine();

                    if (input.equalsIgnoreCase("Quit"))
                    {
                        outputInfoMessage("Exiting...");
                        System.exit(0);
                    }
                    else
                    {
                        if(!StringUtils.isNumeric(input))
                        {
                            outputErrorMessage("The value needs to be an number.");
                        }
                        else if(!input.equals("1") && !input.equals("2"))
                        {
                            outputErrorMessage("The value needs to be 1 or 2.");
                        }
                        else
                        {
                            firstChoice = Integer.valueOf(input);
                            doneFirstChoice = Boolean.TRUE;
                        }
                    }
                }
                else
                {
                    switch(firstChoice)
                    {
                        case 1:
                        {
                            switch(secondChoice)
                            {
                                case 1:
                                {
                                    outputInfoMessage("Please enter a username. Quit to exit.");
                                    break;
                                }
                                case 2:
                                {
                                    outputInfoMessage("Please enter a password. Quit to exit.");
                                    break;
                                }
                                case 3:
                                {
                                    outputInfoMessage("Please enter an age. Quit to exit.");
                                    break;
                                }
                                case 4:
                                {
                                    outputInfoMessage("Please enter a gender (1 - Male, 2 - Female). Quit to exit.");
                                    break;
                                }
                            }

                            String input = bufferedReader.readLine();

                            if (input.equalsIgnoreCase("Quit"))
                            {
                                outputInfoMessage("Exiting...");
                                System.exit(0);
                            }
                            else
                            {
                                // Validate input
                                if((secondChoice == 3 || secondChoice == 4) && !StringUtils.isNumeric(input))
                                {
                                    outputErrorMessage("The value needs to be an number.");
                                }
                                else if(secondChoice == 4 && (!input.equals("1") && !input.equals("2")))
                                {
                                    outputErrorMessage("The value must be a 1 or 2.");
                                }
                                else
                                {
                                    switch(secondChoice)
                                    {
                                        case 1:
                                        {
                                            username = input;
                                            break;
                                        }
                                        case 2:
                                        {
                                            password = input;
                                            break;
                                        }
                                        case 3:
                                        {
                                            age = Integer.valueOf(input);
                                            break;
                                        }
                                        case 4:
                                        {
                                            gender = Integer.valueOf(input);
                                            break;
                                        }
                                    }

                                    if(secondChoice == 4)
                                    {
                                        try
                                        {
                                            createAccount(username, password, age, gender);
                                        }
                                        catch(PreconditionViolationException preconditionViolationExceprion)
                                        {
                                            logger.error(preconditionViolationExceprion.getMessage());
                                        }
                                        catch(AccountService.AccountAlreadyExistsException
                                                accountAlreadyExistsException)
                                        {
                                            logger.error(accountAlreadyExistsException.getMessage());
                                        }
                                        secondChoice = 1;
                                        doneFirstChoice = false;
                                    }
                                    else
                                    {
                                        // If valid
                                        secondChoice++;
                                    }
                                }
                            }
                            break;
                        }
                        case 2:
                        {
                            switch(secondChoice)
                            {
                                case 1:
                                {
                                    outputInfoMessage("Please enter a username. Quit to exit.");
                                    break;
                                }
                                case 2:
                                {
                                    outputInfoMessage("Please enter a password. Quit to exit.");
                                    break;
                                }
                            }

                            String input = bufferedReader.readLine();

                            if (input.equalsIgnoreCase("Quit"))
                            {
                                outputInfoMessage("Exiting...");
                                System.exit(0);
                            }
                            else
                            {
                                switch(secondChoice)
                                {
                                    case 1:
                                    {
                                        username = input;
                                        break;
                                    }
                                    case 2:
                                    {
                                        password = input;
                                        break;
                                    }
                                }

                                if(secondChoice == 2)
                                {
                                    SecurityService.CheckIfAccountExistsResponse checkIfAccountExistsResponse = securityService.
                                            checkIfAccountExists(new SecurityService.CheckIfAccountExistsRequest(username));

                                    outputInfoMessage("Account Exists: " + checkIfAccountExistsResponse.getCheckResult());

                                    SecurityService.LoginResponse loginResponse = securityService.login(new SecurityService.
                                            LoginRequest(username, password));

                                    outputInfoMessage("Login: " + loginResponse.getPasswordResult());

                                    doneFirstChoice = Boolean.FALSE;
                                    secondChoice = 1;
                                }
                                else
                                {
                                    // If valid
                                    secondChoice++;
                                }
                            }


                            break;
                        }
                    }
                }
            }
        }
        catch (IOException e)
        {
            outputErrorMessage("Could not read from the console.");
            System.exit(-1);
        }
        finally
        {
            if (bufferedReader != null)
            {
                try
                {
                    bufferedReader.close();
                }
                catch (IOException e)
                {
                    outputErrorMessage("Could not close the BufferedReader where the input from the console comes" +
                            " from.");
                    System.exit(-1);
                }
            }
        }
        System.exit(0);
    }

    private void createAccount(String username, String password, Integer age, Integer gender) throws
            PreconditionViolationException, AccountService.AccountAlreadyExistsException
    {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setSignedUp(LocalDateTime.now());

        Account account = new Account();
        account.setGender(gender.equals(Integer.valueOf(1)) ? Gender.MALE : Gender.FEMALE);
        account.setUser(user);
        account.setAge(age);

        AccountService.RegisterResponse registerResponse = accountService.register(new AccountService.
                RegisterRequest(account));

        GenerateData.GenerateDevicesResponse generateDevicesResponse = generateData.generateDevices(new
                GenerateDevicesRequest(registerResponse.getAccount()));

        GenerateData.GenerateFitnessDataResponse generateFitnessDataResponse = generateData.
                generateFitnessDeviceData(new GenerateData.GenerateFitnessDataRequest(generateDevicesResponse.
                        getAccount()));

        GenerateData.GenerateGoalsResponse generateGoalsResponse = generateData.
                generateGoals(new GenerateData.GenerateGoalsRequest(generateFitnessDataResponse.getAccount()));

        outputInfoMessage("Details:");
        outputInfoMessage("-User SystemID : " + generateGoalsResponse.getAccount().getUser().getSystemId());
        outputInfoMessage("-Account SystemID : " + generateGoalsResponse.getAccount().getSystemId());

        outputInfoMessage("Devices:");
        for (Device device: generateFitnessDataResponse.getAccount().getDevicesLinkedToAccount())
        {
            outputInfoMessage("-Device: " + device.getClass().getName().substring(device.getClass().getName().
                    lastIndexOf(".") + 1));
            for (FitnessData fitnessData : device.getFitnessDataLinkedToDevice())
            {
                printFitnessData(fitnessData);
            }
        }

        outputInfoMessage("Goals:");
        for (Goal goal : generateFitnessDataResponse.getAccount().getGoalsLinkedToAccount())
        {
            outputInfoMessage("-Goal: " + goal.getClass().getName().substring(goal.getClass().getName().
                    lastIndexOf(".") + 1));
            switch (goal.getClass().getName())
            {
                case "za.co.entelect.fitnessdashboard.domain.objects.goals.Fitness":
                {
                    Fitness fitness = (Fitness) goal;
                    if(fitness.getExerciseGoal() != null)
                    {
                        printFitnessData(fitness.getExerciseGoal());
                    }
                    if(fitness.getHeartRateGoal() != null)
                    {
                        printFitnessData(fitness.getHeartRateGoal());
                    }
                    if(fitness.getStepCountGoal() != null)
                    {
                        printFitnessData(fitness.getStepCountGoal());
                    }
                    break;
                }
                case "za.co.entelect.fitnessdashboard.domain.objects.goals.WeightLoss":
                {
                    WeightLoss weightLoss = (WeightLoss) goal;
                    if(weightLoss.getBodyFatIndicatorGoal() != null)
                    {
                        printFitnessData(weightLoss.getBodyFatIndicatorGoal());
                    }
                    if(weightLoss.getCalorieCounterGoal() != null)
                    {
                        printFitnessData(weightLoss.getCalorieCounterGoal());
                    }
                    if(weightLoss.getWeightGoal() != null)
                    {
                        printFitnessData(weightLoss.getWeightGoal());
                    }
                    break;
                }
            }
        }
    }

    private void printFitnessData(FitnessData fitnessData)
    {
        outputInfoMessage("--Fitness data : " + fitnessData.getClass().getName().substring(fitnessData.
                getClass().getName().lastIndexOf(".") + 1));
        switch (fitnessData.getClass().getName())
        {
            case "za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.BodyFatIndicatorData":
            {
                outputInfoMessage("---Body fat percentage: " + ((BodyFatIndicatorData) fitnessData).
                        getBodyFatPercentage() + "%");
                break;
            }
            case "za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.CalorieCounterData":
            {
                outputInfoMessage("---Substance Consumed: " + ((CalorieCounterData) fitnessData).
                        getSubstanceConsumed());
                outputInfoMessage("---Calorie Count: " + ((CalorieCounterData) fitnessData).
                        getCalorieCount());
                break;
            }
            case "za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.ExerciseTrackerData":
            {
                outputInfoMessage("---Exercise Type: " + ((ExerciseTrackerData) fitnessData).
                        getExerciseType().name());
                outputInfoMessage("---Start Date: " + ((ExerciseTrackerData) fitnessData).getStartDate());
                outputInfoMessage("---End Date: " + ((ExerciseTrackerData) fitnessData).getEndDate());
                break;
            }
            case "za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.HeartRateMonitorData":
            {
                outputInfoMessage("---Heart Rate: " + ((HeartRateMonitorData) fitnessData).getHeartRate() +
                        " bpm");
                break;
            }
            case "za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.PedometerData":
            {
                outputInfoMessage("---Step Count: " + ((PedometerData) fitnessData).getStepCount() + " steps");
                break;
            }
            case "za.co.entelect.fitnessdashboard.domain.objects.fitnessdata.ScaleData":
            {
                outputInfoMessage("---Weight: " + ((ScaleData) fitnessData).getWeight() + "Kg");
                break;
            }
        }
    }

    private static void outputInfoMessage(String message)
    {
        System.out.println(message);
        logger.info(message);
    }

    private static void outputErrorMessage(String message)
    {
        System.err.println(message);
        logger.error(message);
    }

    @Autowired
    private AccountService accountService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private GenerateData generateData;

    // The logger used to output the log statements.
    private static Logger logger = LoggerFactory.getLogger(App.class);
}
