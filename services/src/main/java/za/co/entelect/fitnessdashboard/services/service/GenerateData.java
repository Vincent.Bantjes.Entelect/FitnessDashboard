package za.co.entelect.fitnessdashboard.services.service;

import org.springframework.stereotype.Service;
import za.co.entelect.fitnessdashboard.domain.objects.account.Account;
import za.co.entelect.fitnessdashboard.services.exception.PreconditionViolationException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Service
public interface GenerateData
{
    GenerateDevicesResponse generateDevices(GenerateDevicesRequest generateDeviceDataRequest)
            throws PreconditionViolationException;

    class GenerateDevicesRequest
    {
        public GenerateDevicesRequest(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account for whom the devices are to be generated must be specified.")
        private Account account;
    }

    class GenerateDevicesResponse
    {
        public GenerateDevicesResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account for whom the devices were generated must be specified.")
        private Account account;
    }

    GenerateFitnessDataResponse generateFitnessDeviceData(GenerateFitnessDataRequest generateFitnessDataRequest)
            throws PreconditionViolationException;

    class GenerateFitnessDataRequest
    {
        public GenerateFitnessDataRequest(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that the fitness data needs to be generated for must be specified.")
        private Account account;
    }

    class GenerateFitnessDataResponse
    {
        public GenerateFitnessDataResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account that the fitness data was generated for must be specified.")
        private Account account;
    }

    GenerateGoalsResponse generateGoals(GenerateGoalsRequest generateGoalsRequest) throws
            PreconditionViolationException;

    class GenerateGoalsRequest
    {
        public GenerateGoalsRequest(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account for whom the goals are to be generated must be specified.")
        private Account account;
    }

    class GenerateGoalsResponse
    {
        public GenerateGoalsResponse(Account account)
        {
            this.account = account;
        }

        public Account getAccount() {
            return account;
        }

        public void setAccount(Account account) {
            this.account = account;
        }

        @Valid
        @NotNull(message = "An account for whom the goals were generated must be specified.")
        private Account account;
    }
}
