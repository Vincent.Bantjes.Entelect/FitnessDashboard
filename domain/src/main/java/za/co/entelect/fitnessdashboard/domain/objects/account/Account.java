package za.co.entelect.fitnessdashboard.domain.objects.account;

import za.co.entelect.fitnessdashboard.domain.objects.base.BaseEntity;
import za.co.entelect.fitnessdashboard.domain.objects.devices.Device;
import za.co.entelect.fitnessdashboard.domain.objects.goals.Goal;
import za.co.entelect.fitnessdashboard.domain.objects.security.User;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Access(AccessType.PROPERTY)
@Table(name = "ACCOUNT")
public class Account extends BaseEntity
{
    @OneToMany
    @JoinColumn(name = "ACCOUNT_GOALS")
    public List<Goal> getGoalsLinkedToAccount()
    {
        return goalsLinkedToAccount;
    }

    public void setGoalsLinkedToAccount(List<Goal> goalsLinkedToAccount)
    {
        this.goalsLinkedToAccount = goalsLinkedToAccount;
    }

    @OneToMany
    @JoinColumn(name = "ACCOUNT_DEVICES")
    public List<Device> getDevicesLinkedToAccount()
    {
        return devicesLinkedToAccount;
    }

    private void setDevicesLinkedToAccount(List<Device> devicesLinkedToAccount)
    {
        this.devicesLinkedToAccount = devicesLinkedToAccount;
    }

    @OneToOne
    @JoinColumn(name = "USER")
    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    @Column(name = "AGE")
    public Integer getAge()
    {
        return age;
    }

    public void setAge(Integer age)
    {
        this.age = age;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "Gender")
    public Gender getGender()
    {
        return gender;
    }

    public void setGender(Gender gender)
    {
        this.gender = gender;
    }

    @Valid
    @NotNull(message = "The list of devices needs to be initialized.")
    private List<Goal> goalsLinkedToAccount = new ArrayList();

    @Valid
    @NotNull(message = "The list of devices needs to be initialized.")
    private List<Device> devicesLinkedToAccount = new ArrayList();

    @NotNull(message = "The gender must be specified.")
    private Gender gender;

    @Valid
    @NotNull(message = "The user must be specified.")
    private User user;

    @NotNull(message = "The age for the person must be specified.")
    private Integer age;
}
